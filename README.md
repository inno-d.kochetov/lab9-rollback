# Lab9 - Rollbacks and reliability

![](pics/1.png)

![](pics/2.png)

## Introduction

We know that you've already worked with reliability(at least at Distributed Systems course). So today we are going to talk about rollback implementation in software.(Though I think you've worked with it as well, but maybe don't know about it) ***Let's roll!***

## Rollbacks

If you have a very complicated or very important function that you need to execute it is a good thing to implement Rollbacks there. You are basically saving the state of the execution, and you are returning to it in case of faliure on the next execution state, to try execute next step once again or return the results of at least previous step. One of the most used areas is databases, when you have a chunk of requests and you want all of them to pass, or none of them, so you are sending them in one commit, and if failure happens, your changes from commit are being removed.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab9 - Rollbacks and reliability
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab9-rollback)
2. We are going to write some code today, let's create a maven project, and write some code, but first.
+ Install postgresql(If you have Ubuntu/Mint you should already have it installed)
+ Now, let's open postgresql and create DB like this:
```sql
CREATE DATABASE company;
\c company
CREATE TABLE Employees(id INTEGER PRIMARY KEY, age INT, first TEXT, last TEXT);
```
+ Ok, we can write some code now, create new mvn project and write this code:
```java
import java.sql.*;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try{
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);


            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            System.out.println("Inserting one row....");
            String SQL = "INSERT INTO Employees " +
                    "VALUES (108, 20, 'Rita', 'Tez')";


            System.out.println("Inserting one row....");
            String SQL = "INSERT INTO Employees " +
                    "VALUES (106, 20, 'Rita', 'Tez')";
            stmt.executeUpdate(SQL);
            SQL = "INSERT INTO Employees " +
                    "VALUES (107, 22, 'Sita', 'Singh')";
            stmt.executeUpdate(SQL);

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            String sql = "SELECT id, first, last, age FROM Employees";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try

        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");
    }

    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()){
            //Retrieve by column name
            int id  = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }
}
```
When we will run this code we will add two new users to the DB, now let's experiment a bit with a code. Let's add one new commit and two new users. Add this before Rita and Tez:
```java
            System.out.println("Inserting one row....");
            String SQL = "INSERT INTO Employees " +
                    "VALUES (108, 20, 'Jane', 'Eyre')";

            stmt.executeUpdate(SQL);
            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (109, 20, 'David', 'Rochester')";

            stmt.executeUpdate(SQL);
```
Ok, our table will look like this:

![image supposed to be here](https://i.ibb.co/k2pYbst/lab9-reo.png)

 As you can see Jane was added, because she was in the previous commit, but next commit was rolled back and mr.Rochester hasn't made it into the list of the employees. If we want to carry on the execution we can place each our commit into different try-catch block, this way if something fails, we will just skip failed part and continue the execution from the latest commit nontheless.

## Homework

As a homework you will need to add table `Salary` with parameters: `id`, `employee_id`,`amount` and implement a function, `add_salaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids, LinkedList<Integer> amount)` which will add salaries, based on provided list, like this:
```java
            String SQL = "INSERT INTO Salary " +
                    "VALUES (" + Integer.toString(ids[i]) + "," + 
                                 Integer.toString(employee_ids[i]) + "," + 
                                 Integer.toString(amount[i])+ ")";

            stmt.executeUpdate(SQL);
```
 It shouldn't add salary if there is no id for provided employee or there is already a salary with such id or employee_id. In case of exeception, you should revert just latest transaction, and carry on adding everything which is remaining in the list. P.S. No PL limitation in this lab, you should use Postgres, but you may implement rollbacks in any PL you wish.
