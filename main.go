package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

type PostgresDB struct {
	db *sql.DB
}

func NewPostgresDB() *PostgresDB {
	log.Printf("Connecting to database...")
	connStr := "postgres://postgres:pass123@localhost:5432/company?sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return &PostgresDB{db}
}

func (db *PostgresDB) execTransaction(queries ...string) error {
	// Create a new context, and begin a transaction
	ctx := context.Background()
	tx, err := db.db.BeginTx(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	// `tx` is an instance of `*sql.Tx` through which we can execute our queries

	for _, query := range queries {
		// Here, the query is executed on the transaction instance, and not applied to the database yet
		log.Printf("Executing: %s", query)
		_, err = tx.ExecContext(ctx, query)
		if err != nil {
			// In case we find any error in the query execution, rollback the transaction
			log.Printf("Rolling back failed transaction: \"%s\". Error: %+v\n", query, err)
			err = tx.Rollback()
			if err != nil {
				log.Printf("Rolling back has failed. Error: %+v\n", err)
			}
			return err
		}
	}

	// Finally, if no errors are recieved from the queries, commit the transaction
	// this applies the above changes to our database
	log.Printf("Commiting data here....")
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func (db *PostgresDB) addSalaries(ids, employeeIDs, amount []int) {
	if len(ids) != len(employeeIDs) || len(ids) != len(amount) {
		log.Printf("Aborting addSalaries(), lists length don't match.")
		return
	}
	for idx := range ids {
		q := fmt.Sprintf("INSERT INTO Salary VALUES (%d,%d,%d);", ids[idx], employeeIDs[idx], amount[idx])

		res, _ := db.db.Query(
			fmt.Sprintf("SELECT 1 FROM Employees WHERE id = %d;", ids[idx]))
		if !res.Next() {
			log.Printf("Aborting query: %s. Employees table does not have id = %d", q, ids[idx])
			continue
		}

		res, _ = db.db.Query(
			fmt.Sprintf("SELECT 1 FROM Salary WHERE id = %d OR employee_id = %d;", ids[idx], employeeIDs[idx]))
		if res.Next() {
			log.Printf("Aborting query: %s. Salary table already has an entry with id = %d OR employee_id = %d",
				q, ids[idx], employeeIDs[idx])
			continue
		}

		_ = db.execTransaction(q)
	}
}

func main() {
	db := NewPostgresDB()
	_ = db.execTransaction(
		"CREATE TABLE IF NOT EXISTS Employees(id INTEGER PRIMARY KEY, age INT, first TEXT, last TEXT);")
	_ = db.execTransaction(
		"INSERT INTO Employees VALUES (108, 20, 'Rita', 'Tez');",
		"INSERT INTO Employees VALUES (107, 22, 'Sita', 'Singh')")
	_ = db.execTransaction(
		"CREATE TABLE IF NOT EXISTS Salary(id INTEGER PRIMARY KEY, employee_id INT, amount INT);")

	db.addSalaries(
		[]int{108, 1, 107, 107},
		[]int{108, 1, 107, 107},
		[]int{1000, 1, 2, 3})
}
